package imp.slippy.swing


///**A slippy map that renders to a Swing component.
// * The map is always at an integer zoom level, and has its center at some pixel in a valid tile.
// * All calculations are in tile units; this class does not know about latitude/longitude.*/
//class SwingSlippyMap (initialZoom: Int = 0, initialX: Double = 0.0, initialY: Double = 0.0, private val overlay: SlippyOverlay?, maxRenderThreads: Int = 1) {
//	private val panel = Panel()
//	private val minZoom = 0
//	private val maxZoom = 20
//
//	private val store = TileStore<BufferedImage>({panel.repaint()})
//	private val threads = MapBackgroundRenderer(store, maxRenderThreads)
//	private val changeCache = TileChangeCache(store)
//	private val listeners = ArrayList<SlippyListener>()
//
//	private var centerTile: TilePos
//	/**How many pixels to the right (East) of the center tile's upper-left we are.*/
//	private var offX: Int
//	/**How many pixels down (South) of the center tile's upper-left we are.*/
//	private var offY: Int
//
//
//	init {
//		//set initial position
//		val tileX = Math.floor(initialX).toInt()
//		val tileY = Math.floor(initialY).toInt()
//		this.centerTile = TilePos(initialZoom, tileX, tileY)
//		this.offX = (tileDim * (initialX - MathUtil.roundDown(initialX))).toInt()
//		this.offY = (tileDim * (initialY - MathUtil.roundDown(initialY))).toInt()
//
//		boundLocation()
//		overlay?.setup(this)
//	}
//
//
//	val view: JComponent get() = panel
//	val layers: List<Layer<BufferedImage>> get() = store.allLayers()
//
//	fun addLayer(layer: Layer<BufferedImage>, index: Int = store.layerCount()) {
//		store.addLayer(index, layer)
//		threads.wakeUp()
//	}
//
//	fun removeLayer(index: Int) {
//		store.removeLayer(index)
//	}
//
//	/**The map's current zoom level.*/
//	val zoom: Int get() = centerTile.zoom
//	/**The center of the map, in tile units. */
//	val centerX: Double get() = centerTile.x + (offX / tileDim).toDouble()
//	/**The center of the map, in tile units. */
//	val centerY: Double get() = centerTile.y + (offY / tileDim).toDouble()
//
//
//	fun register(listener: SlippyListener) {
//		listeners.add(listener)
//	}
//
//
//	/**Ensure offsets are within [0, Tile.dim) */
//	private fun normalizeOffsets() {
//		//TODO inefficient, use modulus (figure out what happens with negative numbers)
//		while (offX < 0) {
//			centerTile = centerTile.plus(-1, 0)
//			offX += tileDim
//		}
//		while (offX > tileDim) {
//			centerTile = centerTile.plus(1, 0)
//			offX -= tileDim
//		}
//		while (offY < 0) {
//			centerTile = centerTile.plus(0, -1)
//			offY += tileDim
//		}
//		while (offY > tileDim) {
//			centerTile = centerTile.plus(0, 1)
//			offY -= tileDim
//		}
//
//		boundLocation()
//	}
//
//	/**Ensure the coordinates of our center are valid (don't let the user scroll off the edge of the map). */
//	private fun boundLocation() {
//		if (centerTile.x < 0) {
//			offX = 0
//			centerTile = centerTile.withX(0)
//		} else if (centerTile.x > maxTileIndex(zoom)) {
//			centerTile = centerTile.withX(maxTileIndex(zoom))
//			offX = tileDim - 1
//		}
//		if (centerTile.y < 0) {
//			offY = 0
//			centerTile = centerTile.withY(0)
//		} else if (centerTile.y > maxTileIndex(zoom)) {
//			centerTile = centerTile.withY(maxTileIndex(zoom))
//			offY = tileDim - 1
//		}
//	}
//
//
//	private inner class Panel : JPanel(), ComponentListener, MouseListener, MouseMotionListener, MouseWheelListener {
//		private var dragX: Int = 0
//		private var dragY: Int = 0
//
//		init {
//			isOpaque = true
//			background = Color.black
//			addComponentListener(this)
//			addMouseMotionListener(this)
//			addMouseListener(this)
//			addMouseWheelListener(this)
//		}
//
//		public override fun paintComponent(gg: Graphics) {
//			gg.color = background
//			gg.fillRect(0, 0, width, height)
//
//			val g = gg as Graphics2D
//			g.translate(width / 2, height / 2)
//			val saved = g.transform
//
//			g.translate(-offX, -offY)
//
//			val tileRegionToRender = clippedRegion(zoom, centerTile.x - radX(), centerTile.y - radY(), centerTile.x + radX(), centerTile.y + radY())
//
//			synchronized(store) {
//				tileRegionToRender.forEachPos {tilePos ->
//					for (layer in 0 until store.layerCount()) {
//						val layerTileStore = store.layerStore(layer)
//						val render = layerTileStore.getRendered(TilePos(zoom, tilePos.x, tilePos.y))
//						if (render != null) {
//							val drawX = tileDim * (tilePos.x - centerTile.x)
//							val drawY = tileDim * (tilePos.y - centerTile.y)
//							val k = MathUtil.pow2(zoom - render.first.zoom)
//							val dim = tileDim / k//of the source
//							val srcX = tileDim / k * (tilePos.x % k)
//							val srcY = tileDim / k * (tilePos.y % k)
//							g.drawImage(render.second, drawX, drawY, drawX + tileDim, drawY + tileDim,
//								srcX, srcY, srcX + dim, srcY + dim, null)
//						}
//					}
//				}
//			}
//
//			g.transform = saved
//			overlay?.draw(zoom, centerTile.x, centerTile.y, offX, offY, width, height, g)
//		}
//
//
//		/**Half how many tiles wide this panel is, rounded up. */
//		private fun radX(): Int {
//			return width / tileDim / 2 + if (width % tileDim > 0) 1 else 0
//		}
//
//		/**Half how many tiles tall this panel is, rounded up. */
//		private fun radY(): Int {
//			return height / tileDim / 2 + if (height % tileDim > 0) 1 else 0
//		}
//
//
//		/**Asks the manager for new tiles. */
//		private fun updateRequest() {
//			val newRegion = clippedRegion(zoom, centerTile.x - radX(), centerTile.y - radY(), centerTile.x + radX(), centerTile.y + radY())
//			if(changeCache.changeRegion(newRegion))
//				threads.wakeUp()
//		}
//
//
//		override fun componentResized(componentEvent: ComponentEvent) {
//			updateRequest()
//		}
//
//		override fun mouseWheelMoved(event: MouseWheelEvent) {
//			if (event.wheelRotation == 0)
//				return
//			else if (event.wheelRotation > 0) {
//				if (zoom <= minZoom)
//					return
//				centerTile = centerTile.above()
//				offX /= 2
//				offY /= 2
//			} else {
//				if (zoom >= maxZoom)
//					return
//				centerTile = centerTile.below()
//				offX *= 2
//				offY *= 2
//				normalizeOffsets()
//			}
//
//			updateRequest()
//			repaint()
//		}
//
//		override fun mouseDragged(event: MouseEvent) {
//			offX += dragX - event.x
//			offY += dragY - event.y
//			normalizeOffsets()
//			updateRequest()
//			mouseMoved(event)
//			repaint()
//		}
//
//
//		override fun mouseMoved(event: MouseEvent) {
//			dragX = event.x
//			dragY = event.y
//		}
//
//		override fun componentMoved(componentEvent: ComponentEvent) {}
//		override fun componentShown(componentEvent: ComponentEvent) {}
//		override fun componentHidden(componentEvent: ComponentEvent) {}
//
//		override fun mouseClicked(event: MouseEvent) {
//			if (event.button == 3) {
//				val x = centerTile.x + (event.x - width / 2 + offX) / tileDim.toDouble()
//				val y = centerTile.y + (event.y - height / 2 + offY) / tileDim.toDouble()
//				for (li in listeners)
//					li.onRightClick(x, y)
//			}
//		}
//
//		override fun mousePressed(mouseEvent: MouseEvent) {}
//		override fun mouseReleased(mouseEvent: MouseEvent) {}
//		override fun mouseEntered(mouseEvent: MouseEvent) {}
//		override fun mouseExited(mouseEvent: MouseEvent) {}
//	}
//}
