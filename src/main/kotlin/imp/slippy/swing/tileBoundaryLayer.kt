package imp.slippy.swing

///**Draws lines between tile layers. */
//class TileBoundaryLayer(private val lineWidth: Float, private val lineColor: Color) : Layer<BufferedImage> {
//
//	override val imageClass: Class<BufferedImage> = BufferedImage::class.java
//	override val defaultRetention: BasicTileRetention = BasicTileRetention(1, 1)
//	override fun toString(): String = "TileBoundary[$lineWidth as $lineColor]"
//
//
//	override fun render(pos: TilePos): BufferedImage {
//		val out = BufferedImage(tileDim, tileDim, BufferedImage.TYPE_INT_ARGB)
//		val g = out.graphics as Graphics2D
//		g.color = lineColor
//		g.stroke = BasicStroke(lineWidth)
//		val end = Tiles.dim
//
//		g.drawLine(0, 0, end, 0)
//		g.drawLine(end, 0, end, end)
//		g.drawLine(end, end, 0, end)
//		g.drawLine(0, end, 0, 0)
//
//		return out
//	}
//}
