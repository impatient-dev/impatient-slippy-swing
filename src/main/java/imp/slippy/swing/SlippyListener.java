package imp.slippy.swing;

/**Called when the user interacts with a slippy map.*/
public interface SlippyListener
{
	/**Called when the user right-clicks on a tile.*/
	void onRightClick(double x, double y);
}
