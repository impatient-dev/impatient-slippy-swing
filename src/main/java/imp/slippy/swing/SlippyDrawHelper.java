package imp.slippy.swing;

import imp.slippy.Tiles;
import imp.slippy.WorldCoords;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**Helps with drawing nodes/ways, positioning them on the screen correctly.*/
public class SlippyDrawHelper
{
	protected final Graphics2D g;
	private final int zoom, centerX, centerY, offX, offY;
	/**Whether we are on the first point of a line, as opposed to a later one.*/
	private boolean first;
	/**Used for drawing lines.*/
	private int lastX, lastY;

	/**Sets up a drawer with a graphics object that is offset (offX, offY) pixels to the right/down of tile (zoom, centerX, centerY)'s upper-left corner.*/
	public SlippyDrawHelper(int zoom, int centerX, int centerY, int offX, int offY, Graphics2D g)
	{
		this.zoom = zoom;
		this.centerX = centerX;
		this.centerY = centerY;
		this.offX = offX;
		this.offY = offY;
		this.g = g;
		reset();
	}



	/**Call this to indicate the end of a line and the start of a new one.*/
	public void reset(){first = true;}

	/**Draws an oval around a point. rX and rY are radii in pixels*/
	public void pointOval(double lat, double lon, int rX, int rY)
	{
		int x = xPixel(lat, lon), y = yPixel(lat, lon);
		g.drawOval(x - rX, y - rY, 2 * rX, 2 * rY);
	}
	/**Draws a filled oval around a point. rX and rY are radii in pixels*/
	public void pointOvalFilled(double lat, double lon, int rX, int rY)
	{
		int x = xPixel(lat, lon), y = yPixel(lat, lon);
		g.fillOval(x - rX, y - rY, 2 * rX, 2 * rY);
	}

	/**Draws a polygon centered at a latitude/longitude.*/
	public void polygon(Polygon polygon, double lat, double lon)
	{
		AffineTransform saved = g.getTransform();

		int x = xPixel(lat, lon), y = yPixel(lat, lon);
		g.translate(x, y);
		g.drawPolygon(polygon);

		g.setTransform(saved);
	}

	/**Gives this object a point. Keep feeding it points and it'll draw lines between them.*/
	public void segmentPoint(double lat, double lon)
	{
		int x = xPixel(lat, lon), y = yPixel(lat, lon);
		if(first)
			first = false;
		else
		{
			g.drawLine(lastX, lastY, x, y);

			//modify the stroke's phase so the dashed pattern works properly
			if(g.getStroke() instanceof BasicStroke)
			{
				BasicStroke stroke = (BasicStroke)g.getStroke();
				if(stroke.getDashArray() != null)
				{
					int dx = x - lastX, dy = y - lastY;
					float phaseChange = (float) Math.sqrt(dx * dx + dy * dy);
					float sum = 0;
					for (float value : stroke.getDashArray())
						sum += value;
					float phase = (stroke.getDashPhase() + phaseChange) % sum;
					g.setStroke(new BasicStroke(stroke.getLineWidth(), stroke.getEndCap(), stroke.getLineJoin(), stroke.getMiterLimit(),
							stroke.getDashArray(), phase));
				}
			}
		}

		lastX = x;
		lastY = y;
	}


	/**Draws text centered at a position.*/
	public void text(String text, double lat, double lon)
	{
		int x = xPixel(lat, lon), y = yPixel(lat, lon);
		FontMetrics metrics = g.getFontMetrics();
		int rx = metrics.stringWidth(text) / 2, ry = metrics.getHeight() / 2;
		g.drawString(text, x - rx, y + ry);
	}



	protected int xPixel(double lat, double lon)
	{
		return (int) ((WorldCoords.fullTileX(zoom, lat, lon) - centerX) * Tiles.dim) - offX;
	}

	protected int yPixel(double lat, double lon)
	{
		return (int) ((WorldCoords.fullTileY(zoom, lat, lon) - centerY) * Tiles.dim) - offY;
	}
}
